#version 330

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 color;

out vec2 texCoord0;
out vec3 color0;

uniform vec2 translate;
uniform vec2 size;
uniform vec2 camTrans;
uniform vec4 aspect;

void main() {
    gl_Position = vec4((position * size) + translate - camTrans, 0.0, 1.0) * aspect * vec4(0.25,0.25,1,1);
    texCoord0 = texCoord;
    color0 = color;
}