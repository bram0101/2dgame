#version 330

in vec2 texCoord0;
in vec3 color0;

out vec4 fragColor;

uniform sampler2D sampler;
uniform vec4 color;

void main() 
{
	vec4 textureColor = texture2D(sampler, vec2(texCoord0.y, texCoord0.x));
    
	if(textureColor == 0)
		fragColor = color * vec4(color0, 1);
	else
		fragColor = textureColor * color * vec4(color0, 1);
}