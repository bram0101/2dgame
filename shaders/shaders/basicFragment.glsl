#version 330

in vec2 texCoord0;
in vec3 color0;

out vec4 fragColor;

const float vignetteRaduis = 0.75;
const float vignetteSoftness = 0.45;

uniform sampler2D sampler;
uniform vec4 color;

void main() 
{
	vec4 textureColor = texture2D(sampler, vec2(texCoord0.y, texCoord0.x));
    
    float len = length((gl_FragCoord.xy));
    
    float vignette = smoothstep(vignetteRaduis, vignetteSoftness, len) / 2 + 0.5;
    
    vignette = 1f;
    
	if(textureColor == 0)
		fragColor = (color * vec4(color0, 1)) * (vec4(vignette,vignette,vignette,1));
	else
		fragColor = (textureColor * color * vec4(color0, 1)) * (vec4(vignette,vignette,vignette,1));
}