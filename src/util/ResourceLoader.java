package util;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;

import rendering.Texture;

public class ResourceLoader {

	public static String loadShader(String name) {
		return loadText("shaders/" + name + ".glsl");
	}

	public static String loadText(String file) {
		StringBuilder sb = new StringBuilder();

		BufferedReader br = null;

		try {
			br = new BufferedReader(new InputStreamReader(ResourceLoader.class.getClassLoader().getResourceAsStream(file)));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (br != null) try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

	public static List<String> loadTextToList(String file) {
		List<String> sb = new ArrayList<String>();

		BufferedReader br = null;

		try {
			br = new BufferedReader(new InputStreamReader(ResourceLoader.class.getClassLoader().getResourceAsStream(file)));
			String line;
			while ((line = br.readLine()) != null) {
				sb.add(line);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (br != null) try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb;
	}

	private static Map<String, Texture> textures = new HashMap<String, Texture>();

	public static void destroyTextures() {
		for (Texture t : textures.values()) {
			glDeleteTextures(t.getId());
		}
	}

	public static Texture loadTexture(String fileName) {

		if (textures.containsKey(fileName)) return textures.get(fileName);

		try {
			Logger.info(ResourceLoader.class, "Loading texture: " + fileName);
			BufferedImage img = ImageIO.read(ResourceLoader.class.getClassLoader().getResourceAsStream("textures/" + fileName));
			int bytesPerPixel = 3;
			boolean hasAlpha = img.getColorModel().hasAlpha();
			if (hasAlpha) {
				bytesPerPixel = 4;
				Logger.info(ResourceLoader.class, "Image has alpha");
			}
			int[] pixels = new int[img.getWidth() * img.getHeight()];
			img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			ByteBuffer buffer = BufferUtils.createByteBuffer(img.getWidth() * img.getHeight() * bytesPerPixel);
			for (int x = 0; x < img.getWidth(); x++) {
				for (int y = 0; y < img.getHeight(); y++) {
					int pixel = pixels[y * img.getWidth() + x];
					buffer.put((byte) ((pixel >> 16) & 0xFF));
					buffer.put((byte) ((pixel >> 8) & 0xFF));
					buffer.put((byte) (pixel & 0xFF));
					if (hasAlpha) {
						buffer.put((byte) ((pixel >> 24) & 0xFF));
					}
				}
			}
			buffer.flip();
			int textureID = glGenTextures();
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL12.GL_TEXTURE_MAX_LEVEL, 2);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL11.GL_TRUE);

			glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? GL_RGBA8 : GL_RGB8, img.getWidth(), img.getHeight(), 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
					buffer);
			Texture t = new Texture(textureID);
			textures.put(fileName, t);
			return t;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
	
	public static Texture loadTextureWithoutRepeat(String fileName) {

		if (textures.containsKey(fileName)) return textures.get(fileName);

		try {
			Logger.info(ResourceLoader.class, "Loading texture: " + fileName);
			BufferedImage img = ImageIO.read(ResourceLoader.class.getClassLoader().getResourceAsStream("textures/" + fileName));
			int bytesPerPixel = 3;
			boolean hasAlpha = img.getColorModel().hasAlpha();
			if (hasAlpha) {
				bytesPerPixel = 4;
				Logger.info(ResourceLoader.class, "Image has alpha");
			}
			int[] pixels = new int[img.getWidth() * img.getHeight()];
			img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			ByteBuffer buffer = BufferUtils.createByteBuffer(img.getWidth() * img.getHeight() * bytesPerPixel);
			for (int x = 0; x < img.getWidth(); x++) {
				for (int y = 0; y < img.getHeight(); y++) {
					int pixel = pixels[y * img.getWidth() + x];
					buffer.put((byte) ((pixel >> 16) & 0xFF));
					buffer.put((byte) ((pixel >> 8) & 0xFF));
					buffer.put((byte) (pixel & 0xFF));
					if (hasAlpha) {
						buffer.put((byte) ((pixel >> 24) & 0xFF));
					}
				}
			}
			buffer.flip();
			int textureID = glGenTextures();
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL12.GL_TEXTURE_MAX_LEVEL, 2);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL11.GL_TRUE);

			glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? GL_RGBA8 : GL_RGB8, img.getWidth(), img.getHeight(), 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
					buffer);
			Texture t = new Texture(textureID);
			textures.put(fileName, t);
			return t;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
	
	public static Texture loadTextureWithBlur(String fileName) {

		if (textures.containsKey(fileName)) return textures.get(fileName);

		try {
			Logger.info(ResourceLoader.class, "Loading texture: " + fileName);
			BufferedImage img = ImageIO.read(ResourceLoader.class.getClassLoader().getResourceAsStream("textures/" + fileName));
			int bytesPerPixel = 3;
			boolean hasAlpha = img.getColorModel().hasAlpha();
			if (hasAlpha) {
				bytesPerPixel = 4;
				Logger.info(ResourceLoader.class, "Image has alpha");
			}
			int[] pixels = new int[img.getWidth() * img.getHeight()];
			img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			ByteBuffer buffer = BufferUtils.createByteBuffer(img.getWidth() * img.getHeight() * bytesPerPixel);
			for (int x = 0; x < img.getWidth(); x++) {
				for (int y = 0; y < img.getHeight(); y++) {
					int pixel = pixels[y * img.getWidth() + x];
					buffer.put((byte) ((pixel >> 16) & 0xFF));
					buffer.put((byte) ((pixel >> 8) & 0xFF));
					buffer.put((byte) (pixel & 0xFF));
					if (hasAlpha) {
						buffer.put((byte) ((pixel >> 24) & 0xFF));
					}
				}
			}
			buffer.flip();
			int textureID = glGenTextures();
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL12.GL_TEXTURE_MAX_LEVEL, 2);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL11.GL_TRUE);

			glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? GL_RGBA8 : GL_RGB8, img.getWidth(), img.getHeight(), 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
					buffer);
			Texture t = new Texture(textureID);
			textures.put(fileName, t);
			return t;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static Texture loadTexture(String fileName, BufferedImage img) {

		if (textures.containsKey(fileName)) return textures.get(fileName);

		try {
			int bytesPerPixel = 3;
			boolean hasAlpha = img.getColorModel().hasAlpha();
			if (img.getColorModel().hasAlpha()) bytesPerPixel = 4;
			int[] pixels = new int[img.getWidth() * img.getHeight()];
			img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			ByteBuffer buffer = BufferUtils.createByteBuffer(img.getWidth() * img.getHeight() * bytesPerPixel);
			for (int x = 0; x < img.getWidth(); x++) {
				for (int y = 0; y < img.getHeight(); y++) {
					int pixel = pixels[y * img.getWidth() + x];
					buffer.put((byte) ((pixel >> 16) & 0xFF));
					buffer.put((byte) ((pixel >> 8) & 0xFF));
					buffer.put((byte) (pixel & 0xFF));
					if (img.getColorModel().hasAlpha()) buffer.put((byte) ((pixel >> 24) & 0xFF));
				}
			}
			buffer.flip();
			int textureID = glGenTextures();
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? GL_RGBA8 : GL_RGB8, img.getWidth(), img.getHeight(), 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
					buffer);
			Texture t = new Texture(textureID);
			textures.put(fileName, t);
			return t;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static Texture loadTextureWithBlur(String fileName, BufferedImage img) {

		if (textures.containsKey(fileName)) return textures.get(fileName);

		try {
			int bytesPerPixel = 3;
			boolean hasAlpha = img.getColorModel().hasAlpha();
			if (img.getColorModel().hasAlpha()) bytesPerPixel = 4;
			int[] pixels = new int[img.getWidth() * img.getHeight()];
			img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			ByteBuffer buffer = BufferUtils.createByteBuffer(img.getWidth() * img.getHeight() * bytesPerPixel);
			for (int x = 0; x < img.getWidth(); x++) {
				for (int y = 0; y < img.getHeight(); y++) {
					int pixel = pixels[y * img.getWidth() + x];
					buffer.put((byte) ((pixel >> 16) & 0xFF));
					buffer.put((byte) ((pixel >> 8) & 0xFF));
					buffer.put((byte) (pixel & 0xFF));
					if (img.getColorModel().hasAlpha()) buffer.put((byte) ((pixel >> 24) & 0xFF));
				}
			}
			buffer.flip();
			int textureID = glGenTextures();
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? GL_RGBA8 : GL_RGB8, img.getWidth(), img.getHeight(), 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
					buffer);
			Texture t = new Texture(textureID);
			textures.put(fileName, t);
			return t;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	
}
