package util;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.GL11;

import rendering.Mesh;
import rendering.Shader;

public class OpenGLUtil {

	private static boolean FBOEnabled;

	public static void init() {
		Logger.info(OpenGLUtil.class, "Using openGL: " + getOpenGLVersion());
		glClearColor(0, 0, 0, 0);
		glFrontFace(GL_CW);
		glCullFace(GL_FRONT);
		glEnable(GL_CULL_FACE);

		glEnable(GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glViewport(0, 0, (int) WindowUtil.getWidth(), (int) WindowUtil.getHeight());
	}

	public static void clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	public static void destroy() {
		Mesh.destroy();
		Shader.destroy();
		ResourceLoader.destroyTextures();
	}

	public static String getOpenGLVersion() {
		return glGetString(GL_VERSION);
	}

	public static void setTextures(boolean enabled) {
		if (enabled)
			glEnable(GL_TEXTURE_2D);
		else glDisable(GL_TEXTURE_2D);
	}

	public static boolean isTextures() {
		return GL11.glIsEnabled(GL_TEXTURE_2D);
	}

	public static void update() {
		GL11.glViewport(0, 0, (int) WindowUtil.getWidth(), (int) WindowUtil.getHeight());
	}

}
