package util;

import java.util.ArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector2f;

public class Input {

	public static int ESCAPE = Keyboard.KEY_ESCAPE;
	public static int GPESCAPE = 2;

	public static int RETURN = Keyboard.KEY_RETURN;
	public static int GPRETURN = 3;

	public static int USE = Keyboard.KEY_W;
	public static int GPUSE = 1;

	public static int JUMP = Keyboard.KEY_SPACE;
	public static int GPJUMP = 0;

	public static int SCREENSHOT = Keyboard.KEY_F2;
	public static int GPSCREENSHOT = 6;

	public static int NUM_PADCODES = 0;
	public static final int NUM_KEYCODES = 256;
	public static final int NUM_MOUSEBUTTONS = 5;

	private static ArrayList<Integer> currentPad = new ArrayList<Integer>();
	private static ArrayList<Integer> downPad = new ArrayList<Integer>();
	private static ArrayList<Integer> upPad = new ArrayList<Integer>();

	private static ArrayList<Integer> currentKeys = new ArrayList<Integer>();
	private static ArrayList<Integer> downKeys = new ArrayList<Integer>();
	private static ArrayList<Integer> upKeys = new ArrayList<Integer>();

	private static ArrayList<Integer> currentMouse = new ArrayList<Integer>();
	private static ArrayList<Integer> downMouse = new ArrayList<Integer>();
	private static ArrayList<Integer> upMouse = new ArrayList<Integer>();

	private static Vector2f mouseD;
	private static Controller[] c;
	private static int gp = -1;
	private static Vector2f gpInitNAxis;

	public static void init() {
		int cc = 0;
		try {
			Controllers.create();
			cc = Controllers.getControllerCount();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		c = new Controller[cc];

		for (int i = 0; i < cc; i++) {
			c[i] = Controllers.getController(i);
			if (c[i].getName().indexOf("Controller") > -1) {
				gp = i;
			}
		}
		if (gp == -1) {
			System.out.println("No gamepad controller exists.");
		} else {
			int bc = c[gp].getButtonCount();
			NUM_PADCODES = bc;
			int ac = c[gp].getAxisCount();
			System.out.println("The controller has " + bc + " buttons and " + ac + " axis.");
			for (int i = 0; i < bc; i++) {
				System.out.println(c[gp].getButtonName(i));
			}
		}

	}

	public static void update() {

		if (gp > -1) {
			//for (int i = 0; i < c[gp].getButtonCount(); i++) {
			//	if (c[gp].isButtonPressed(i)) {
			//		System.out.println(c[gp].getButtonName(i) + " is pressed.");
			//	}
			//}
			upPad.clear();

			for (int i = 0; i < NUM_PADCODES; i++)
				if (!getPad(i) && currentPad.contains(i)) upPad.add(i);

			downPad.clear();

			for (int i = 0; i < NUM_KEYCODES; i++)
				if (getPad(i) && !currentPad.contains(i)) downPad.add(i);

			currentPad.clear();

			for (int i = 0; i < NUM_KEYCODES; i++)
				if (getPad(i)) currentPad.add(i);

		}
		Controllers.poll();

		upKeys.clear();

		for (int i = 0; i < NUM_KEYCODES; i++)
			if (!getKey(i) && currentKeys.contains(i)) upKeys.add(i);

		downKeys.clear();

		for (int i = 0; i < NUM_KEYCODES; i++)
			if (getKey(i) && !currentKeys.contains(i)) downKeys.add(i);

		currentKeys.clear();

		for (int i = 0; i < NUM_KEYCODES; i++)
			if (getKey(i)) currentKeys.add(i);

		upMouse.clear();

		for (int i = 0; i < NUM_MOUSEBUTTONS; i++)
			if (!getMouse(i) && currentMouse.contains(i)) upMouse.add(i);

		downMouse.clear();

		for (int i = 0; i < NUM_MOUSEBUTTONS; i++)
			if (getMouse(i) && !currentMouse.contains(i)) downMouse.add(i);

		currentMouse.clear();
		for (int i = 0; i < NUM_MOUSEBUTTONS; i++)
			if (getMouse(i)) currentMouse.add(i);

		int dx = Mouse.getDX();
		int dy = Mouse.getDY();
		if (gp > -1) {
			if (gpInitNAxis == null) {
				gpInitNAxis = new Vector2f(c[gp].getXAxisValue(), c[gp].getYAxisValue());
			}
			if (c[gp].getXAxisValue() > 0.335 && c[gp].getXAxisValue() != gpInitNAxis.x) {
				dx = 10;
				gpInitNAxis.x = 0;
			}
			if (c[gp].getXAxisValue() < -0.335 && c[gp].getXAxisValue() != gpInitNAxis.x) {
				dx = -10;
				gpInitNAxis.x = 0;
			}
			if (c[gp].getYAxisValue() > 0.335 && c[gp].getYAxisValue() != gpInitNAxis.y) {
				dy = -10;
				gpInitNAxis.y = 0;
			}
			if (c[gp].getYAxisValue() < -0.335 && c[gp].getYAxisValue() != gpInitNAxis.y) {
				dy = 10;
				gpInitNAxis.y = 0;
			}
		}
		mouseD = new Vector2f(dx, dy);

	}

	public static boolean getPad(int keyCode) {
		if (gp > -1) if (keyCode < c[gp].getButtonCount()) return c[gp].isButtonPressed(keyCode);
		return false;
	}

	public static boolean getKey(int keyCode) {
		if (keyCode == ESCAPE && gp > -1) return Keyboard.isKeyDown(ESCAPE) || c[gp].isButtonPressed(GPESCAPE);
		if (keyCode == RETURN && gp > -1) return Keyboard.isKeyDown(RETURN) || c[gp].isButtonPressed(GPRETURN);
		if (keyCode == USE && gp > -1) return Keyboard.isKeyDown(USE) || c[gp].isButtonPressed(GPUSE);
		if (keyCode == JUMP && gp > -1) return Keyboard.isKeyDown(JUMP) || c[gp].isButtonPressed(GPJUMP);
		return Keyboard.isKeyDown(keyCode);
	}

	public static boolean getKeyDown(int keyCode) {
		if (keyCode == ESCAPE && gp > -1) return downKeys.contains(ESCAPE) || downPad.contains(GPESCAPE);
		if (keyCode == RETURN && gp > -1) return downKeys.contains(RETURN) || downPad.contains(GPRETURN);
		if (keyCode == USE && gp > -1) return downKeys.contains(USE) || downPad.contains(GPUSE);
		if (keyCode == JUMP && gp > -1) return downKeys.contains(JUMP) || downPad.contains(GPJUMP);
		if (keyCode == SCREENSHOT && gp > -1) return downKeys.contains(SCREENSHOT) || downPad.contains(GPSCREENSHOT);
		return downKeys.contains(keyCode);
	}

	public static boolean getKeyUp(int keyCode) {
		if (keyCode == ESCAPE && gp > -1) return upKeys.contains(ESCAPE) || upPad.contains(GPESCAPE);
		if (keyCode == RETURN && gp > -1) return upKeys.contains(RETURN) || upPad.contains(GPRETURN);
		if (keyCode == USE && gp > -1) return upKeys.contains(USE) || upPad.contains(GPUSE);
		if (keyCode == JUMP && gp > -1) return upKeys.contains(JUMP) || upPad.contains(GPJUMP);
		return upKeys.contains(keyCode);
	}

	public static boolean getMouse(int mouseButton) {
		return Mouse.isButtonDown(mouseButton);
	}

	public static boolean getMouseDown(int mouseButton) {
		return downMouse.contains(mouseButton);
	}

	public static boolean getMouseUp(int mouseButton) {
		return upMouse.contains(mouseButton);
	}

	public static Vector2f getMouse() {
		return new Vector2f(Mouse.getX(), Mouse.getY());
	}

	public static Vector2f getMouseD() {
		if (mouseD == null) mouseD = new Vector2f(Mouse.getDX(), Mouse.getDY());
		return new Vector2f(mouseD.x, mouseD.y);
	}

	public static void grabMouse() {
		Mouse.setGrabbed(true);
	}

	public static void releaseMouse() {
		Mouse.setGrabbed(false);
	}

	public static boolean isGrabbed() {
		return Mouse.isGrabbed();
	}
}
