package util;

import java.nio.IntBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Vector2f;

import rendering.Vertex;

public class ArrayUtil {

	public static IntBuffer toIntBuffer(int... values) {
		IntBuffer buffer = BufferUtils.createIntBuffer(values.length);
		buffer.put(values);
		buffer.flip();
		return buffer;
	}

	public static String[] removeEmptyStrings(String[] tokens) {
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < tokens.length; i++)
			if (!tokens[i].equals("")) result.add(tokens[i]);
		String[] res = new String[result.size()];
		result.toArray(res);
		return res;
	}

	public static int[] toIntArray(Integer[] data) {
		int[] res = new int[data.length];
		for (int i = 0; i < data.length; i++)
			res[i] = data[i].intValue();
		return res;
	}

	public static int[] add(int[] orig, int[] add) {
		int[] res = new int[orig.length + add.length];

		for (int i = 0; i < orig.length; i++) {
			res[i] = orig[i];
		}
		for (int i = 0; i < add.length; i++) {
			res[i + orig.length] = add[i];
		}

		return res;
	}
	
	public static Vertex[] add(Vertex[] orig, Vertex[] add) {
		Vertex[] res = new Vertex[orig.length + add.length];

		for (int i = 0; i < orig.length; i++) {
			res[i] = orig[i];
		}
		for (int i = 0; i < add.length; i++) {
			res[i + orig.length] = add[i];
		}

		return res;
	}
	
	public static Vertex[] addVertices(Vertex[] orig, Vertex[] add) {
		for(int i = 0; i < orig.length; i++) {
			orig[i] = orig[i].add(add[i]);
		}
		return orig;
	}
	
	public static Vertex[] addVertices(Vertex[] vertices, Vector2f devide) {
		for(int i = 0; i < vertices.length; i++) {
			vertices[i] = vertices[i].add(devide);
		}
		return vertices;
	}

}
