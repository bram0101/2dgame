package util;

import org.lwjgl.opengl.Display;

public class WindowUtil {

	public static float getWidth() {
		return Display.getWidth();
	}

	public static float getHeight() {
		return Display.getHeight();
	}

	public static float getAspectRatio() {
		return getHeight() / getWidth();
	}

}
