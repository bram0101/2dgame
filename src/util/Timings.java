package util;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;

public class Timings {
	private static long lastFPS;
	private static long lastFrame;
	private static int tempFPS;
	public static double delta = 1;
	public static double deltaSeconds;
	public static int FPS;

	public static void update() {
		long time = getTime();
		delta = time - lastFrame;
		deltaSeconds = delta / 1000;
		lastFrame = time;
		if (lastFPS == 0) lastFPS = time;
		while (getTime() - lastFPS > 1000) {
			Display.setTitle("JGame : " + FPS + "FPS");
			lastFPS += 1000;
			FPS = tempFPS;
			tempFPS = 0;
		}
		tempFPS++;
	}

	public static long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
}
