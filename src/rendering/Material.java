package rendering;

import org.lwjgl.util.vector.Quaternion;

public class Material {

	private Texture texture;
	private Quaternion color;

	public Material(Texture texture) {
		this.texture = texture;
		this.color = new Quaternion(1, 1, 1, 1);
	}

	public Material(Texture texture, Quaternion color) {
		this.texture = texture;
		this.color = color;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public Quaternion getColor() {
		return color;
	}

	public void setColor(Quaternion color) {
		this.color = color;
	}

	public Material clone() {
		return new Material(texture, new Quaternion(color.x, color.y, color.z, color.w));
	}

}
