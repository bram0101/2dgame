package rendering;

import util.ArrayUtil;

public class MeshData {

	public Vertex[] vertices;
	public int[] indices;

	public MeshData(Vertex[] vertices, int[] indices) {
		this.vertices = vertices;
		this.indices = indices;
	}

	public MeshData() {
		vertices = new Vertex[0];
		indices = new int[0];
	}

	public void add(Vertex[] vertices, int[] indices) {
		this.vertices = ArrayUtil.add(this.vertices, vertices);
		this.indices = ArrayUtil.add(this.indices, indices);
	}

	public void add(MeshData data) {
		add(data.vertices, data.indices);
	}

}
