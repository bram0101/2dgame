package rendering;

import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDeleteShader;
import static org.lwjgl.opengl.GL20.glGetProgram;
import static org.lwjgl.opengl.GL20.glGetShader;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector2f;

import util.Logger;

public class Shader {

	private static Set<Shader> shaders = new HashSet<Shader>();
	
	public static GuiShader guiShader;
	public static BasicShader basicShader;
	
	public static void init() {
		guiShader = new GuiShader();
		basicShader = new BasicShader();
	}

	public static void destroy() {
		for (Shader s : shaders) {
			glDeleteProgram(s.program);
			for (Integer i : s.fshader)
				glDeleteShader(i.intValue());
			for (Integer i : s.vshader)
				glDeleteShader(i.intValue());
		}
	}

	private int program;
	private Set<Integer> vshader;
	private Set<Integer> fshader;
	private Map<String, Integer> uniforms;

	public Shader() {
		program = glCreateProgram();
		uniforms = new HashMap<String, Integer>();
		vshader = new HashSet<Integer>();
		fshader = new HashSet<Integer>();

		if (program == 0) {
			Logger.err(getClass(), "Shader creation failed: Could not find valid memory location in constructor");
		}
	}

	public void bind() {
		glUseProgram(program);
	}

	public static void unbind() {
		glUseProgram(0);
	}

	public void addUniform(String uniform) {
		int uniformLocation = glGetUniformLocation(program, uniform);
		if (uniformLocation == 0xFFFFFFFF) {
			Logger.err(getClass(), "Couldn't find uniform: " + uniform);
			new Exception().printStackTrace();
			System.exit(-1);
		}

		uniforms.put(uniform, uniformLocation);
	}

	public void setUniformi(String uniformName, int value) {
		glUniform1i(uniforms.get(uniformName), value);
	}

	public void setUniformF(String uniformName, float value) {
		glUniform1f(uniforms.get(uniformName), value);
	}

	public void setUniform(String uniformName, Vector2f value) {
		glUniform2f(uniforms.get(uniformName), value.x, value.y);
	}

	public void setUniform(String uniformName, Quaternion value) {
		glUniform4f(uniforms.get(uniformName), value.x, value.y, value.z, value.w);
	}

	public void addVertexShader(String text) {
		vshader.add(Integer.valueOf(addProgram(text, GL_VERTEX_SHADER)));
	}

	public void addGeometryShader(String text) {
		addProgram(text, GL_GEOMETRY_SHADER);
	}

	public void addFragmentShader(String text) {
		fshader.add(Integer.valueOf(addProgram(text, GL_FRAGMENT_SHADER)));
	}

	public void compileShader() {
		glLinkProgram(program);
		if (glGetProgram(program, GL_LINK_STATUS) == 0) {
			Logger.err(getClass(), glGetShaderInfoLog(program, 1024));
			System.exit(-1);
		}

		glValidateProgram(program);

		if (glGetProgram(program, GL_VALIDATE_STATUS) == 0) {
			Logger.err(getClass(), glGetShaderInfoLog(program, 1024));
			System.exit(-1);
		}
	}

	private int addProgram(String text, int type) {
		int shader = glCreateShader(type);
		if (shader == 0) {
			Logger.err(getClass(), "Shader creation failed: Could not find valid memory location at adding shader");
			System.exit(-1);
		}

		glShaderSource(shader, text);
		glCompileShader(shader);

		if (glGetShader(shader, GL_COMPILE_STATUS) == 0) {
			Logger.err(getClass(), glGetShaderInfoLog(shader, 1024) + "     on program: " + shader);
			System.exit(-1);
		}

		glAttachShader(program, shader);
		return shader;
	}

}
