package rendering;

import org.lwjgl.util.vector.Vector2f;

public class Camera {

	private Vector2f translation;
	private Vector2f rotation;

	public Camera() {
		translation = new Vector2f(0, 0);
		rotation = new Vector2f(0, 0);
	}

	public Camera(Vector2f translation) {
		this.translation = translation;
		rotation = new Vector2f(0, 0);
	}

	public Camera(Vector2f translation, Vector2f rotation) {
		this.translation = translation;
		this.rotation = rotation;
	}

	public Vector2f getTranslation() {
		return translation;
	}

	public void setTranslation(Vector2f translation) {
		this.translation = translation;
	}

	public Vector2f getRotation() {
		return rotation;
	}

	public void setRotation(Vector2f rotation) {
		this.rotation = rotation;
	}

	public void move(float x, float y) {
		translation.translate(x, y);
	}

	public void move(Vector2f amount) {
		move(amount.x, amount.y);
	}

}
