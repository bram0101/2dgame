package rendering;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import util.WindowUtil;

public class GuiTarget {

	private static Vector2f location;
	private static Mesh mesh;

	public static void init() {
		if (mesh == null) mesh = new Mesh();
		mesh.setShader(Shader.guiShader);
		Vector3f color = new Vector3f(1, 1, 1);
		mesh.setVertices(new Vertex[] { new Vertex(-1, 0.25f, color, new Vector2f(0, 1)), new Vertex(-1, -0.25f, color, new Vector2f(0, 0)),
				new Vertex(1, 0.25f, color, new Vector2f(1, 1)), new Vertex(1f, -0.25f, color, new Vector2f(1, 0)),

				new Vertex(-0.25f, 1f, color, new Vector2f(0, 1)), new Vertex(-0.25f, -1f, color, new Vector2f(0, 0)),
				new Vertex(0.25f, 1f, color, new Vector2f(1, 1)), new Vertex(0.25f, -1f, color, new Vector2f(1, 0)) }, new int[] { 0, 2, 1, 2, 3, 1, 4, 6, 5,
				6, 7, 5 });
	}

	public static void draw() {
		mesh.setSize(new Vector2f(WindowUtil.getAspectRatio() * 0.1f, 0.1f));
		mesh.draw();
	}

	public static void setLocation(int x, int y) {
		location.set(x, y);
	}

	public static Vector2f getLocation() {
		return location;
	}

}
