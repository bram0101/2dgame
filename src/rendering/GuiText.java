package rendering;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import util.Logger;
import util.ResourceLoader;
import util.WindowUtil;

public class GuiText {

	private static Mesh mesh;
	public static final Font NORMAL = new Font("Arial", Font.PLAIN, 30);
	private static Map<Integer, Glyph> glyphs = new HashMap<Integer, Glyph>();

	public static void init() {
		int width = 512;
		int height = 512;
		int gridSize = 16;
		int chunkSize = width / gridSize;
		float charWidth = 1f / ((float) width) * chunkSize;
		float charHeight = 1f / ((float) height) * chunkSize;
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) img.getGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setFont(NORMAL);
		g2.setColor(Color.WHITE);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				img.setRGB(x, y, new Color(0, 0, 1, 0).getRGB());
			}
		}
		for (int i = 0; i < 128; i++) {
			char c = Character.valueOf((char) i).charValue();
			// int x = (i - (int)(i / 4096));
			// int y = i / 4096 - x;
			// int i = x + (y * gridSize);
			// int x = (i - ((int)(i / gridSize)));
			// int y = ((int)(i / gridSize)) - x;
			int y = i / gridSize;
			int x = i - (y * gridSize);
			x = x * chunkSize;
			y = y * chunkSize;
			g2.drawString(String.valueOf(c), x, y + chunkSize / 1.25f);
			glyphs.put(i, new Glyph(((float) x) / ((float) width), ((float) y) / ((float) height), (((float) x) / ((float) width)) + charWidth,
					(((float) y) / ((float) height)) + charHeight, (float) g2.getFontMetrics().getStringBounds(String.valueOf(c), g2).getWidth()));
		}
		Texture t = ResourceLoader.loadTexture("font2", img);
		if (mesh == null) mesh = new Mesh();
		mesh.setMaterial(new Material(t, new Quaternion(1, 1, 1, 1)));
		mesh.setShader(Shader.guiShader);
	}

	public static int getWidth(String s, float size) {
		int totalWidth = 0;
		int tI = 0;
		for (char c : s.toCharArray()) {
			if (c == '\n') {
				if (tI > totalWidth) totalWidth = tI;
				tI = 0;
				continue;
			}
			tI += glyphs.get((int) c).charWidth * (size / 32);
		}
		if (tI > totalWidth) totalWidth = tI;

		return totalWidth;
	}

	public static void drawString(String s, float x, float y, float size, Vector3f color) {
		drawString(s, x, y, size, new Quaternion(color.x, color.y, color.z, 1));
	}

	public static void drawString(String s, float x, float y, float size, Quaternion color) {
		if (mesh == null) mesh = new Mesh();
		drawStringShadow(s, x + size / 16, y + size / 16, size, new Quaternion(color.x, color.y, color.z, color.w));
		MeshData md = new MeshData();
		int offset = 0;
		float oldX = x;
		for (char c : s.toCharArray()) {
			if (c == '\n') {
				x = oldX;
				y += size + 2;
				continue;
			}
			Glyph g = glyphs.get((int) c);
			if (g == null) {
				Logger.err(GuiText.class, "Can't find glyph for char: " + c);
			} else {
				md.add(drawChar(g, x, y, size, offset));
			}
			offset += 4;
			x += g.charWidth * (size / 32);
		}
		mesh.setVertices(md);
		mesh.getMaterial().setColor(color);
		mesh.draw();
	}

	public static void drawStringShadow(String s, float x, float y, float size, Quaternion color) {
		if (mesh == null) mesh = new Mesh();
		MeshData md = new MeshData();
		int offset = 0;
		float oldX = x;
		for (char c : s.toCharArray()) {
			if (c == '\n') {
				x = oldX;
				y += size + 2;
				continue;
			}
			Glyph g = glyphs.get((int) c);
			if (g == null) {
				Logger.err(GuiText.class, "Can't find glyph for char: " + c);
			} else {
				md.add(drawChar(g, x, y, size, offset));
			}
			offset += 4;
			x += g.charWidth * (size / 32);
		}
		mesh.setVertices(md);
		color.scale(0.5f);
		mesh.getMaterial().setColor(color);
		mesh.draw();
	}

	public static void drawString(String s, float x, float y, float size) {
		drawString(s, x, y, size, new Vector3f(1, 1, 1));
	}

	public static void drawStringCentered(String s, float x, float y, float size) {
		drawString(s, x - ((float) getWidth(s, size)) / 2f, y - size / 2f, size, new Vector3f(1, 1, 1));
	}

	private static MeshData drawChar(Glyph g, float x, float y, float size, int offset) {
		MeshData md = new MeshData();
		float xx = (x / WindowUtil.getWidth()) * 2 - 1f;
		float yy = (y / WindowUtil.getHeight()) * 2 - 1f;
		float xx2 = ((x + size) / WindowUtil.getWidth()) * 2 - 1f;
		float yy2 = ((y + size) / WindowUtil.getHeight()) * 2 - 1f;
		Vector3f color = new Vector3f(1, 1, 1);
		Vertex[] vertices = new Vertex[] { new Vertex(xx, yy2, color, new Vector2f(g.u, g.v2)), new Vertex(xx, yy, color, new Vector2f(g.u, g.v)),
				new Vertex(xx2, yy2, color, new Vector2f(g.u2, g.v2)), new Vertex(xx2, yy, color, new Vector2f(g.u2, g.v)) };
		int[] indices = new int[] { 0 + offset, 2 + offset, 1 + offset, 2 + offset, 3 + offset, 1 + offset };
		md.add(vertices, indices);
		return md;
	}

	public static class Glyph {
		public float u;
		public float v;
		public float u2;
		public float v2;
		public float charWidth;

		public Glyph(float u, float v, float u2, float v2, float charWidth) {
			this.u = u + 0.001f;
			this.v = v + 0.001f;
			this.u2 = u2 - 0.001f;
			this.v2 = v2 - 0.0001f;
			this.charWidth = charWidth;
		}
	}

}
