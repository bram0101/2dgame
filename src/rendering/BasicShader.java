package rendering;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector2f;

import util.ResourceLoader;
import util.WindowUtil;
import base.GameEngine;

public class BasicShader extends Shader {

	public BasicShader() {
		super();

		addVertexShader(ResourceLoader.loadShader("basicVertex"));
		addFragmentShader(ResourceLoader.loadShader("basicFragment"));
		compileShader();
		addUniform("translate");
		addUniform("size");
		addUniform("color");
		addUniform("camTrans");
		addUniform("aspect");
	}

	public void updateUniforms(Material material, Vector2f location, Vector2f size) {
		setUniform("translate", location);
		setUniform("size", size);
		setUniform("color", material.getColor());
		setUniform("camTrans", GameEngine.game.getCamera().getTranslation());
		setUniform("aspect", new Quaternion(WindowUtil.getAspectRatio(), 1, 1, 1));
	}
}
