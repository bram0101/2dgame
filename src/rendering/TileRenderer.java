package rendering;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import util.ResourceLoader;
import world.Chunk;
import world.Tile;
import world.Tiles;

public class TileRenderer {

	private Mesh[] tileMeshes = new Mesh[Tiles.values().length];

	public void renderBatch(Tile[] batch, Tiles m, Chunk w) {
		Mesh tileMesh = tileMeshes[m.id - 1];
		boolean changed = w.isChanged(m);
		if (tileMesh == null) {
			tileMesh = new Mesh();
			changed = true;
		}
		if (changed) {
			Vector3f color = new Vector3f(1, 1, 1);
			MeshData md = new MeshData();
			int offset = 0;

			for (Tile t : batch) {
				if (t != null) {
					MeshData tmd = new MeshData();
					tmd.add(new Vertex[] { new Vertex(t.getX(), t.getY() + 1, color, new Vector2f(0, 0)),
							new Vertex(t.getX(), t.getY(), color, new Vector2f(0, 1)), new Vertex(t.getX() + 1, t.getY() + 1, color, new Vector2f(1, 0)),
							new Vertex(t.getX() + 1, t.getY(), color, new Vector2f(1, 1)) }, new int[] { 0 + offset, 1 + offset, 2 + offset, 2 + offset,
							1 + offset, 3 + offset });
					offset += 4;
					md.add(tmd);
				}
			}

			tileMesh.setVertices(md);
			tileMesh.setMaterial(new Material(ResourceLoader.loadTextureWithoutRepeat("tiles/" + m.texture + ".png")));
		}
		tileMesh.draw();
		tileMeshes[m.id - 1] = tileMesh;
		//Logger.infoD("Rendering: " + w.getX() + " " + w.getY());
	}

}
