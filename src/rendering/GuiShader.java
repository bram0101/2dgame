package rendering;

import org.lwjgl.util.vector.Vector2f;

import util.ResourceLoader;

public class GuiShader extends Shader {
	
	public GuiShader() {
		super();

		addVertexShader(ResourceLoader.loadShader("guiVertex"));
		addFragmentShader(ResourceLoader.loadShader("guiFragment"));
		compileShader();
		addUniform("translate");
		addUniform("size");
		addUniform("color");
	}

	public void updateUniforms(Material material, Vector2f location, Vector2f size) {
		setUniform("translate", location);
		setUniform("size", size);
		setUniform("color", material.getColor());
	}
}
