package rendering;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STREAM_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

import java.util.HashSet;
import java.util.Set;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import util.ArrayUtil;
import util.ResourceLoader;

public class Mesh {

	public static Set<Mesh> meshes = new HashSet<Mesh>();

	public static void destroy() {
		for (Mesh m : meshes) {
			glDeleteBuffers(m.ibo);
			glDeleteBuffers(m.vbo);
		}
	}

	public int getVbo() {
		return vbo;
	}

	public void setVbo(int vbo) {
		this.vbo = vbo;
	}

	public int getIbo() {
		return ibo;
	}

	public void setIbo(int ibo) {
		this.ibo = ibo;
	}

	protected int vbo;
	protected int ibo;
	protected int ibo_size;
	protected Material material;
	public Vector2f translation;
	public Vector2f size;
	protected Shader shader;

	public Mesh() {
		translation = new Vector2f();
		size = new Vector2f(1, 1);
		shader = Shader.basicShader;
		vbo = glGenBuffers();
		ibo = glGenBuffers();
		ibo_size = 0;
		material = new Material(ResourceLoader.loadTexture("white.png"), new Quaternion(1, 1, 1, 1));
		meshes.add(this);
		Vector3f color = new Vector3f(1, 1, 1);
		setVertices(new Vertex[] { new Vertex(-1, 1, color, new Vector2f(0, 1)), new Vertex(-1, -1, color, new Vector2f(0, 0)),
				new Vertex(1, 1f, color, new Vector2f(1, 1)), new Vertex(1f, -1f, color, new Vector2f(1, 0)) }, new int[] { 0, 2, 1, 2, 3, 1 });
	}

	public Mesh(MeshData mesh) {
		this();
		setVertices(mesh.vertices, mesh.indices);
	}

	public void setVertices(Vertex[] vertices, int[] indices) {
		ibo_size = indices.length;

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, Vertex.createFlippedBuffer(vertices), GL_STREAM_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ArrayUtil.toIntBuffer(indices), GL_STREAM_DRAW);
	}

	public void setVertices(MeshData data) {
		setVertices(data.vertices, data.indices);
	}

	public void draw() {

		// add shader update and bind
		shader.bind();
		if (shader instanceof BasicShader) {
			((BasicShader) shader).updateUniforms(material, translation, size);
		}
		if (shader instanceof GuiShader) {
			((GuiShader) shader).updateUniforms(material, translation, size);
		}

		material.getTexture().bind();
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glVertexAttribPointer(0, 3, GL_FLOAT, false, Vertex.SIZE * 4, 0);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, Vertex.SIZE * 4, 8);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, Vertex.SIZE * 4, 16);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glDrawElements(GL_TRIANGLES, ibo_size, GL_UNSIGNED_INT, 0);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Shader getShader() {
		return shader;
	}

	public void setShader(Shader s) {
		shader = s;
	}

	public Vector2f getTranslation() {
		return translation;
	}

	public void setTranslation(Vector2f translation) {
		this.translation = translation;
	}

	public Vector2f getSize() {
		return size;
	}

	public void setSize(Vector2f size) {
		this.size = size;
	}

}
