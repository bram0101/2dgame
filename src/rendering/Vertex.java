package rendering;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Vertex {

	public static final int SIZE = 7;

	private Vector2f pos;
	private Vector2f texCoord;
	private Vector3f color = new Vector3f(1, 1, 1);

	public Vertex(float x, float y) {
		this(new Vector2f(x, y));
	}

	public Vertex(float x, float y, float r, float g, float b) {
		this(new Vector2f(x, y), new Vector3f(r, g, b));
	}

	public Vertex(float x, float y, Vector3f color) {
		this(new Vector2f(x, y), color);
	}

	public Vertex(float x, float y, Vector3f color, Vector2f textCoord) {
		this(new Vector2f(x, y), textCoord, color);
	}

	public Vertex(Vector2f pos) {
		this(pos, new Vector2f(0, 0));
	}

	public Vertex(Vector2f pos, Vector2f texCoord) {
		this.pos = pos;
		this.texCoord = texCoord;
	}

	public Vertex(Vector2f pos, Vector2f texCoord, Vector3f color) {
		this(pos, texCoord);
		this.color = color;
	}

	public Vertex(Vector2f pos, Vector3f color) {
		this(pos, new Vector2f(0, 0), color);
	}

	public Vector2f getPos() {
		return pos;
	}

	public void setPos(Vector2f pos) {
		this.pos = pos;
	}

	public Vector2f getTexCoord() {
		return texCoord;
	}

	public void setTexCoord(Vector2f texCoord) {
		this.texCoord = texCoord;
	}

	public Vector3f getColor() {
		return color;
	}

	public void setColor(Vector3f color) {
		this.color = color;
	}

	public static FloatBuffer createFlippedBuffer(Vertex[] vertices) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length * SIZE);

		for (int i = 0; i < vertices.length; i++) {
			buffer.put(vertices[i].pos.x);
			buffer.put(vertices[i].pos.y);
			buffer.put(vertices[i].texCoord.x);
			buffer.put(vertices[i].texCoord.y);
			buffer.put(vertices[i].color.x);
			buffer.put(vertices[i].color.y);
			buffer.put(vertices[i].color.z);
		}

		buffer.flip();

		return buffer;
	}

	public Vertex add(Vertex vertex) {
		pos.translate(vertex.pos.x, vertex.pos.y);
		return this;
	}

	public Vertex add(Vector2f loc) {
		pos.translate(loc.x, loc.y);
		return this;
	}
}
