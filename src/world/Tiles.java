package world;

public enum Tiles {

	GRASS(1, "grass"), DIRT(2, "dirt"), STONE(3, "stone"), WOOD(4, "wood");

	public int id;
	public String texture;

	Tiles(int id, String texture) {
		this.id = id;
		this.texture = texture;
	}

}
