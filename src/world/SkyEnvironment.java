package world;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import rendering.Material;
import rendering.Mesh;
import rendering.Vertex;
import util.ResourceLoader;

public class SkyEnvironment extends Environment {

	private Mesh sky;

	@Override
	public void init() {
		sky = new Mesh();
		Vector3f color = new Vector3f(1, 1, 1);
		sky.setVertices(new Vertex[] { new Vertex(-800, 8, color, new Vector2f(0, 1)), new Vertex(-800, -8, color, new Vector2f(0, 0)),
				new Vertex(800, 8, color, new Vector2f(100, 1)), new Vertex(800f, -8, color, new Vector2f(100, 0)) }, new int[] { 0, 1, 2, 2, 1, 3 });
		sky.setMaterial(new Material(ResourceLoader.loadTextureWithBlur("environment/sky.png")));
	}

	@Override
	public void render() {
		sky.draw();
	}

	@Override
	public void destroy() {
	}

}
