package world;

import java.util.Random;

import util.Logger;
import base.GameEngine;

public class World {

	public final int worldWidth = 360;
	public final int worldHeight = 80;

	private Chunk[] chunks = new Chunk[(worldWidth / Chunk.ChunkWidth) * (worldHeight / Chunk.ChunkHeight)];

	public World() {

		for (int x = 0; x < 320; x++) {
			for (int y = 0; y < 80; y++) {
				setTile(x, y, Tiles.values()[new Random().nextInt(Tiles.values().length)].id);
			}
		}

	}

	public void render() {
		int cx = (int) (GameEngine.game.getCamera().getTranslation().x / Chunk.ChunkWidth);
		int cy = (int) (GameEngine.game.getCamera().getTranslation().y / Chunk.ChunkHeight);
		if (getChunk(cx, cy) != null) getChunk(cx, cy).render();

		if (getChunk(cx - 1, cy) != null) getChunk(cx - 1, cy).render();
		if (getChunk(cx - 1, cy - 1) != null) getChunk(cx - 1, cy - 1).render();
		if (getChunk(cx - 1, cy + 1) != null) getChunk(cx - 1, cy + 1).render();
		if (getChunk(cx + 1, cy) != null) getChunk(cx + 1, cy).render();
		if (getChunk(cx + 1, cy - 1) != null) getChunk(cx + 1, cy - 1).render();
		if (getChunk(cx + 1, cy + 1) != null) getChunk(cx + 1, cy + 1).render();
		if (getChunk(cx, cy + 1) != null) getChunk(cx, cy + 1).render();
		if (getChunk(cx, cy - 1) != null) getChunk(cx, cy - 1).render();
	}

	public void tick() {
		if (getChunkRelativeToWorld((int) GameEngine.game.getCamera().getTranslation().x, (int) GameEngine.game.getCamera().getTranslation().y) != null)
			getChunkRelativeToWorld((int) GameEngine.game.getCamera().getTranslation().x, (int) GameEngine.game.getCamera().getTranslation().y).tick();
	}

	public void setTile(int x, int y, int id) {
		if (getChunkRelativeToWorld(x, y) == null) generateChunk((int) x / Chunk.ChunkWidth, y / Chunk.ChunkHeight);
		if (id == 0)
			getChunkRelativeToWorld(x, y).setTile(x % Chunk.ChunkWidth, y % Chunk.ChunkHeight, null);
		else getChunkRelativeToWorld(x, y).setTile(x % Chunk.ChunkWidth, y % Chunk.ChunkHeight, new Tile(x, y, id));
	}

	public Tile getTile(int x, int y) {
		return getChunkRelativeToWorld(x, y).getTile(x % Chunk.ChunkWidth, y % Chunk.ChunkHeight);
	}

	public int getChunkX(int x) {
		return x / Chunk.ChunkWidth;
	}

	public int getChunkY(int y) {
		return y / Chunk.ChunkHeight;
	}

	private void setChunk(int x, int y, Chunk c) {
		chunks[x * (worldHeight / Chunk.ChunkHeight) + y] = c;
	}

	public void generateChunk(int x, int y) {
		if (getChunk(x, y) == null) {
			setChunk(x, y, new Chunk(this, x, y));
			Logger.infoD("Generating chunk " + x + " " + y);
		}
	}

	public Chunk getChunk(int x, int y) {
		if (x < 0 || x > (worldWidth / Chunk.ChunkWidth) || y < 0 || y > (worldHeight / Chunk.ChunkHeight)) return null;
		return chunks[x * (worldHeight / Chunk.ChunkHeight) + y];
	}

	public Chunk getChunkRelativeToWorld(int x, int y) {
		// Logger.infoD("Getting Chunk: " + x + " " + y);
		return getChunk(x / Chunk.ChunkWidth, y / Chunk.ChunkHeight);
	}

}
