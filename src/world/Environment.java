package world;

public abstract class Environment {
	
	public abstract void init();
	
	public abstract void render();
	
	public abstract void destroy();
	
}
