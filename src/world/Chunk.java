package world;

import rendering.TileRenderer;
import util.Logger;
import util.MathHelper;

public class Chunk {

	public static final int ChunkWidth = 16;
	public static final int ChunkHeight = 16;

	private World w;

	private int x;
	private int y;

	private Tile[][] tiles = new Tile[Tiles.values().length + 1][ChunkWidth * ChunkHeight];
	private boolean[] tileChanged = new boolean[Tiles.values().length];

	private TileRenderer renderer;

	public Chunk(World w, int x, int y) {
		this.w = w;
		this.x = x;
		this.y = y;
		renderer = new TileRenderer();
	}

	public void render() {
		for (Tiles m : Tiles.values()) {
			Tile[] batch = tiles[m.id];
			if (batch != null) {
				if (batch.length > 0) {
					renderer.renderBatch(batch, m, this);
				}
			}
		}
	}

	public void tick() {
		for (Tiles m : Tiles.values()) {
			Tile[] batch = tiles[m.id];
			if (batch != null) {
				if (batch.length > 0) {
					for (Tile t : batch) {
						if (t != null) t.tick();
					}
				}
			}
		}
	}

	public void setTile(int x, int y, Tile tile) {
		if (tile == null) {
			for (Tile[] batch : tiles) {
				x = MathHelper.clamp(x, 0, ChunkWidth);
				y = MathHelper.clamp(y, 0, ChunkHeight);
				if (batch[x * ChunkHeight + y] != null) {
					tileChanged[batch[x * ChunkHeight + y].getId() - 1] = true;
					batch[x * ChunkHeight + y] = null;
					break;
				}
			}
		} else {
			//Logger.infoD("Placing block at " + x + " " + y + " with world coords " + tile.getX() + " " + tile.getY());
			Tile[] batch = tiles[tile.getId()];
			x = MathHelper.clamp(x, 0, ChunkWidth);
			y = MathHelper.clamp(y, 0, ChunkHeight);
			batch[x * ChunkHeight + y] = tile;
			tileChanged[tile.getId() - 1] = true;
		}

	}

	public Tile getTile(int x, int y) {
		for (Tile[] batch : tiles) {
			x = MathHelper.clamp(x, 0, ChunkWidth);
			y = MathHelper.clamp(y, 0, ChunkHeight);
			if (batch[x * ChunkHeight + y] != null) { return batch[x * ChunkHeight + y]; }
		}
		return null;
	}

	public boolean isChanged(Tiles m) {
		return tileChanged[m.id - 1];
	}

	public World getW() {
		return w;
	}

	public void setW(World w) {
		this.w = w;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public TileRenderer getRenderer() {
		return renderer;
	}

	public void setRenderer(TileRenderer renderer) {
		this.renderer = renderer;
	}

}
