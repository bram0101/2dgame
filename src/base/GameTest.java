package base;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;

import rendering.Camera;
import rendering.GuiTarget;
import rendering.GuiText;
import util.Input;
import util.Timings;
import world.Environment;
import world.SkyEnvironment;
import world.World;

public class GameTest extends Game {

	private Environment env;
	private World w;

	@Override
	public void init() {
		camera = new Camera();
		env = new SkyEnvironment();
		env.init();
		Input.grabMouse();
		w = new World();
		GuiTarget.init();
	}

	@Override
	public void render() {
		env.render();
		w.render();
		GuiText.drawString("CamX: " + camera.getTranslation().x + "\nCamY: " + camera.getTranslation().y, 10, 10, 32);
		GuiTarget.draw();
	}

	@Override
	public void logic() {
		w.tick();
	}

	@Override
	public void input() {
		if (Input.getMouseDown(0)) Input.grabMouse();
		if (Input.getKeyDown(Keyboard.KEY_ESCAPE)) Input.releaseMouse();
		if (Input.getKeyDown(Keyboard.KEY_RETURN)) GameEngine.pendStop();

		if (Input.getKey(Input.USE)) {
			w.setTile((int) camera.getTranslation().x, (int) camera.getTranslation().y, 0);
		}

		if (Input.isGrabbed()) getCamera().move((Vector2f) Input.getMouseD().scale((float) Timings.deltaSeconds));

	}

	@Override
	public void destroy() {

	}

}
