package base;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

import rendering.GuiText;
import rendering.Mesh;
import rendering.ScreenShot;
import rendering.Shader;
import util.Input;
import util.MemoryManager;
import util.OpenGLUtil;
import util.ResourceLoader;
import util.ThreadHandler;
import util.Timings;

public class GameEngine implements Runnable {

	public static boolean RUNNING = false;
	private static boolean pendStop = false;
	public static Game game;
	private static Game nextGame;

	public static void START() {
		if (!RUNNING) {
			RUNNING = true;
			ThreadHandler.createThread("gameThread", false, new GameEngine());
		}
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				GameEngine.pendStop();
			}

		}));
	}

	public static void STOP() {
		if (RUNNING) {
			RUNNING = false;
			ThreadHandler.stopThread("gameThread");
		}
	}

	public static void pendStop() {
		pendStop = true;
	}

	@Override
	public void run() {
		try {
			PixelFormat pixelFormat = new PixelFormat(8, 8, 0, 8);
			// ContextAttribs contextAtributes = new ContextAttribs(3, 2).withForwardCompatible(true).withProfileCore(true);
			Display.setDisplayMode(new DisplayMode(1280, 720));
			Display.setTitle("JGame");
			Display.setResizable(true);
			Display.create(pixelFormat);
			Keyboard.create();
			Mouse.create();
			init();
			Timings.update();
			while (!pendStop) {
				if (Display.isCloseRequested()) pendStop();
				Timings.update();
				update();
				Display.update();
				Display.sync(60);
			}
			cleanUp();
			Keyboard.destroy();
			Mouse.destroy();
			Display.destroy();
			STOP();
		} catch (Exception ex) {
			ex.printStackTrace();
			STOP();
		}
	}

	public static void nextGame(Game g) {
		nextGame = g;
	}

	private void init() {
		OpenGLUtil.init();
		Input.init();
		Shader.init();
		GuiText.init();
		game = new GameTest();
		game.init();
	}

	private void update() {
		MemoryManager.update();
		if (Display.wasResized()) {
			OpenGLUtil.update();
		}
		OpenGLUtil.clear();
		if (nextGame != null) {
			System.out.println("Setting new game to: " + nextGame);
			game.destroy();
			game = nextGame;
			nextGame = null;
			game.init();
		}
		// render
		game.input();
		game.logic();
		game.render();

		if (Input.getKeyDown(Input.SCREENSHOT)) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");
			Date date = new Date();
			ScreenShot.take(new File(Launcher.GAME_DIR, "screenshots/screenshot_" + dateFormat.format(date) + ".png"));
		}
		Input.update();
	}

	private void cleanUp() {
		game.destroy();
		OpenGLUtil.destroy();
		Mesh.destroy();
		Shader.destroy();
		ResourceLoader.destroyTextures();
	}

}
