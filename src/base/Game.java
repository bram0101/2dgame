package base;

import rendering.Camera;


public abstract class Game {

	protected Camera camera;
	public static float secondsPlayed;

	public abstract void init();

	public abstract void render();

	public abstract void logic();

	public abstract void input();

	public abstract void destroy();

	public Camera getCamera() {
		return camera;
	}

}
